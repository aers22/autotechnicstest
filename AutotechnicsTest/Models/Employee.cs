﻿namespace AutotechnicsTest.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int DepartamentId { get; set; }
    }
}
