﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace AutotechnicsTest
{
    public class ConfigReader
    {
        static IConfigurationRoot config = null;

        static ConfigReader()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            config = builder.Build();
        }

        public static string ConnectionString
        {
            get
            {
                return config.GetSection("EmployeeRepositorySettings").GetValue<string>("DbConnection");
            }
        }


        public static string ReadAllCommand
        {
            get
            {
                return config.GetSection("EmployeeRepositorySettings").GetValue<string>("ReadAllCommand");
            }
        }

        public static string ReadByDepartamentCommand
        {
            get
            {
                return config.GetSection("EmployeeRepositorySettings").GetValue<string>("ReadByDepartamentCommand");
            }
        }
    }
}
