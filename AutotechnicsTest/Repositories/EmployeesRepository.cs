﻿using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using Dapper;
using System.Data;
using AutotechnicsTest.Models;

namespace AutotechnicsTest.Repositories
{
    public class EmployeesRepository
    {

        private IDbConnection dbConnection = null;

        public EmployeesRepository()
        {
            dbConnection = new SqlConnection(ConfigReader.ConnectionString);
        }
        public List<Employee> GetAll()
        {
            string sql = ConfigReader.ReadAllCommand;
            var queryResult = dbConnection.Query<Employee>(sql);
            return queryResult.ToList();
        }

        public List<Employee> GetByDepId(int depId)
        {
            string sql = ConfigReader.ReadByDepartamentCommand;
            var queryResult = dbConnection.Query<Employee>(sql, new { DepId = depId });
            return queryResult.ToList();
        }
    }
}
