﻿using System.Collections.Generic;
using AutotechnicsTest.Models;
using AutotechnicsTest.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace AutotechnicsTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        EmployeesRepository employeeRepository = null;

        public EmployeesController()
        {
            employeeRepository = new EmployeesRepository();
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> GetAll()
        {
            IList<Employee> employees = employeeRepository.GetAll();

            return Ok(employees);
        }

        [HttpGet("{depId}")]
        public ActionResult<IEnumerable<string>> GetByDepId(int depId)
        {
            IList<Employee> employee = employeeRepository.GetByDepId(depId);

            if (employee != null)
            {
                return Ok(employee);
            }
            return NotFound();
        }
    }
}
